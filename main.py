porcentagem_total = 10.16
porcentagem_proporcional_mes = porcentagem_total / 12.
dicionario_meses = {
    "janeiro": 1,
    "fevereiro": 2,
    "março": 3,
    "abril": 4,
    "maio": 5,
    "junho": 6,
    "julho": 7,
    "agosto": 8,
    "setembro": 9,
    "outubro": 10,
    "novembro": 11,
    "dezembro": 12
}

meses = ""
for mes in dicionario_meses:
    if meses == "":
        meses = mes
    else:
        meses = meses + ", " + mes


print("Se você foi admitido em 2021, por favor digite o mês de admissão: \n"+ meses)
print("Se foi admitido no ano anterior, digite 'janeiro'")
mes = input("Mês de admissão: ")

mes_admissao = {}
try:
    mes_admissao = dicionario_meses[mes]
except:
    print("Mês de admissão inválido. Rode o programa novamente inserindo um mês válido.")
    quit()

print(mes_admissao)

multiplicador_percentual = 12 - mes_admissao + 1
percentual_relacionado_admissao = round(multiplicador_percentual * porcentagem_proporcional_mes, 2)

print("\n\n##############################################################################")
print("O percentual proporcional do reajuste é de: "+str(percentual_relacionado_admissao)+"%")

salario = 0.
try:
    salario = float(input("Agora digite o seu salário na época do mês de admissão \n (utilizando ponto ao invés de vírgula no separador de centavos: 99.99)"))
except:
    print("Salário inválido. Rode o programa novamente inserindo um valor válido.")
    quit()


valor_reajuste = round((salario / 100) * percentual_relacionado_admissao, 2)


print("\n\n##############################################################################")
print("Cálculo do reajuste:")
print("R$ "+str(salario)+" + "+str(percentual_relacionado_admissao)+"% = R$ "+str(salario) + " + R$ "+str(valor_reajuste)+" = R$ "+str(salario+valor_reajuste))
print("##############################################################################")
print("Cálculo do retroativo:")

total_retroativo = round(valor_reajuste*(multiplicador_percentual+2),2)

print("(R$ "+str(valor_reajuste)+") * "+str(multiplicador_percentual+2)+ " ("+mes+" 2021 a fevereiro 2022) = R$ "+str(total_retroativo))
print("##############################################################################")
print("Salário bruto do mês de fevereiro: R$ "+str(salario) +" + R$ "+ str(total_retroativo)+" = R$ "+str(salario+valor_reajuste+total_retroativo))